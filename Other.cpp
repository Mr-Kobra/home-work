#include <iostream>
#include <ctime>
#include <iomanip>
#include "Other.h"
using namespace std;

int MatrixSize{}, MinElement{}, MaxElement{};
int** CreateMatrix()
{
	//srand(time(0));
	cout << "Input matrix size: " << endl;
	cin >> MatrixSize;
	while (MatrixSize < 1) {
		cout << "Input number > 0" << endl;
		cin >> MatrixSize;
	}
	cout << "Input min value: " << endl;
	cin >> MinElement;
	cout << "Input max value: " << endl;
	cin >> MaxElement;
	int** Matrix = new int* [MatrixSize];
	for (int i = 0; i < MatrixSize; i++)
	{
		Matrix[i] = new int[MatrixSize];
		for (int j = 0; j < MatrixSize; j++)
		{
			Matrix[i][j] = (rand() % (MaxElement - MinElement)) + MinElement;
		}
	}
	PrintMatrix(Matrix);
	return Matrix;
}
int getSizeMatrix() {
	return MatrixSize;
}
void PrintMatrix(int** Matrix)
{
	for (int i = 0; i < MatrixSize; i++)
	{
		for (int j = 0; j < MatrixSize; j++)
		{
			cout << setw(5) << Matrix[i][j];
		}
		cout << endl;
	}
}
int* GetMaxDiagonal(int** Matrix)
{
	int* maxElement = new int[2]{ 0, 0 };
	for (int i = 0; i < MatrixSize; i++)
	{
		for (int j = 0; j < MatrixSize; j++)
		{
			if (i == j && Matrix[maxElement[0]][maxElement[1]] < Matrix[i][j]) {
				maxElement[0] = i;
				maxElement[1] = j;
			}
		}
	}
	return maxElement;
}
void PrintMaxDiagonal(int** Matrix)
{
	int* maxElement = GetMaxDiagonal(Matrix);
	cout << "Max element on main diagonal: " << Matrix[maxElement[0]][maxElement[1]] << endl;
	cout << "Max element row: " << maxElement[0] << endl;
	cout << "Max element column: " << maxElement[1] << endl;
}
int* GetMaxUnderDiagonal(int** Matrix)
{
	int* maxElement = new int[2]{ MatrixSize - 1, MatrixSize - 1 };
	for (int i = 0; i < MatrixSize; i++)
	{
		for (int j = 0; j < MatrixSize; j++)
		{
			if (i > j && Matrix[maxElement[0]][maxElement[1]] < Matrix[i][j]) {
				maxElement[0] = i;
				maxElement[1] = j;
			}
		}
	}
	return maxElement;
}
void PrintMaxUnderDiagonal(int** Matrix)
{
	int* maxElement = GetMaxUnderDiagonal(Matrix);
	cout << "Max element under main diagonal: " << Matrix[maxElement[0]][maxElement[1]] << endl;
	cout << "Max element row: " << maxElement[0] << endl;
	cout << "Max element column: " << maxElement[1] << endl;
}
void ReplaceElement(int* maxElementDiagonal, int* maxElementUnderDiagonal, int** Matrix)
{
	Matrix[maxElementDiagonal[0]][maxElementDiagonal[1]] = Matrix[maxElementUnderDiagonal[0]][maxElementUnderDiagonal[1]];
}
void getMatrixWithoutRowAndCol(int** matrix, int size, int row, int col, int** newMatrix) {
	int offsetRow = 0;
	int offsetCol = 0;
	for (int i = 0; i < size - 1; i++) {
		if (i == row) {
			offsetRow = 1;
		}

		offsetCol = 0;
		for (int j = 0; j < size - 1; j++) {
			if (j == col) {
				offsetCol = 1;
			}

			newMatrix[i][j] = matrix[i + offsetRow][j + offsetCol];
		}
	}
}
int matrixDet(int** matrix, int size) {
	int det = 0;
	int degree = 1;

	if (size == 1) {
		return matrix[0][0];
	}

	if (size == 2) {
		return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
	}

	int** newMatrix = new int* [size - 1];
	for (int i = 0; i < size - 1; i++) {
		newMatrix[i] = new int[size - 1];
	}

	for (int j = 0; j < size; j++) {
		getMatrixWithoutRowAndCol(matrix, size, 0, j, newMatrix);

		det = det + (degree * matrix[0][j] * matrixDet(newMatrix, size - 1));

		degree = -degree;
	}

	for (int i = 0; i < size - 1; i++) {
		delete[] newMatrix[i];
	}
	delete[] newMatrix;

	return det;
}