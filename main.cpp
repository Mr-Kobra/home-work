#include <iostream>
#include "Other.h"
using namespace std;

int main()
{
	char MenuItem{};
	int** BaseMatrix{};
	int det = 0;
	int size = 0;
	do {
		cout << "1 - Create matrix" << endl;
		cout << "2 - Print matrix" << endl;
		cout << "3 - Find max element on main diagonal" << endl;
		cout << "4 - Find max element under main diagonal" << endl;
		cout << "5 - Replace max element on main diagonal with max element under main diagonal" << endl;
		cout << "6 - Getting matrix determinant" << endl;
		cout << "- > ";
		cin >> MenuItem;
		system("cls");
		switch (MenuItem)
		{
		case '1':
			BaseMatrix = CreateMatrix();
			size = getSizeMatrix();
			break;
		case '2':
			PrintMatrix(BaseMatrix);
			break;
		case '3':
			PrintMaxDiagonal(BaseMatrix);
			break;
		case '4':
			PrintMaxUnderDiagonal(BaseMatrix);
			break;
		case '5':
			ReplaceElement(GetMaxDiagonal(BaseMatrix), GetMaxUnderDiagonal(BaseMatrix), BaseMatrix);
			PrintMatrix(BaseMatrix);
			break;
		case '6':
			det = matrixDet(BaseMatrix, size);
			cout << "�atrix determinant:" << det << endl;
			break;
		case '0':
			exit(0); 
			break;
		default:
			cout << "You entered invalid number" << endl;
			break;
		}
	} while (true);
}
