#include <iostream>
#include <ctime>
#include <iomanip>
#include "Module.h"
using namespace std;

int MatrixSize{}, MinElement{}, MaxElement{};
int** CreateMatrix()
{
	//srand(time(0));
	cout << "Input matrix size: " << endl;
	if (MatrixSize == 0) {
		cin >> MatrixSize;
		while (MatrixSize < 1) {
			cout << "Input number > 0" << endl;
			cin >> MatrixSize;
		}
	}
	if (MinElement == 0) {
		cout << "Input min value: " << endl;
		cin >> MinElement;
	}
	if (MaxElement == 0) {
		cout << "Input max value: " << endl;
		cin >> MaxElement;
	}
	int** Matrix = new int* [MatrixSize];
	for (int i = 0; i < MatrixSize; i++)
	{
		Matrix[i] = new int[MatrixSize];
		for (int j = 0; j < MatrixSize; j++)
		{
			Matrix[i][j] = (rand() % (MaxElement - MinElement)) + MinElement;
		}
	}
	return Matrix;
}

void PrintMatrix(int** Matrix)
{
	for (int i = 0; i < MatrixSize; i++)
	{
		for (int j = 0; j < MatrixSize; j++)
		{
			cout << setw(5) << Matrix[i][j];
		}
		cout << endl;
	}
}
int* GetMaxDiagonal(int** Matrix)
{
	int* maxElement = new int[2]{ 0, 0 };
	for (int i = 0; i < MatrixSize; i++)
	{
		for (int j = 0; j < MatrixSize; j++)
		{
			if (i == j && Matrix[maxElement[0]][maxElement[1]] < Matrix[i][j]) {
				maxElement[0] = i;
				maxElement[1] = j;
			}
		}
	}
	return maxElement;
}
void PrintMaxDiagonal(int** Matrix)
{
	int* maxElement = GetMaxDiagonal(Matrix);
	cout << "Max element on main diagonal: " << Matrix[maxElement[0]][maxElement[1]] << endl;
	cout << "Max element row: " << maxElement[0] << endl;
	cout << "Max element column: " << maxElement[1] << endl;
}
int* GetMaxUnderDiagonal(int** Matrix)
{
	int* maxElement = new int[2]{ MatrixSize - 1, MatrixSize - 1 };
	for (int i = 0; i < MatrixSize; i++)
	{
		for (int j = 0; j < MatrixSize; j++)
		{
			if (i > j && Matrix[maxElement[0]][maxElement[1]] < Matrix[i][j]) {
				maxElement[0] = i;
				maxElement[1] = j;
			}
		}
	}
	return maxElement;
}
void ReduceTriangleMatrx(int** Matrix)
{
	for (int i = 0; i < MatrixSize; i++)
	{
		for (int j = 0; j < MatrixSize; j++)
		{
			if (j > i) {
				Matrix[i][j] = 0;
			}
		}
	}
}
int** SumMatrix(int** MatrixA, int** MatrixB)
{
	int** temp = new int*[MatrixSize];
	for (int i = 0; i < MatrixSize; i++)
	{
		temp[i] = new int[MatrixSize];
		for (int j = 0; j < MatrixSize; j++)
		{
			temp[i][j] = MatrixA[i][j] + MatrixB[i][j];
		}
	}
	return temp;
}