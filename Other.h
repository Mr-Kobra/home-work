#pragma once
int** CreateMatrix();
void PrintMatrix(int** Matrix);
int* GetMaxDiagonal(int** Matrix);
void PrintMaxDiagonal(int** Matrix);
int* GetMaxUnderDiagonal(int** Matrix);
void PrintMaxUnderDiagonal(int** Matrix);
void ReplaceElement(int* maxElementDiagonal, int* maxElementUnderDiagonal, int** Matrix);
int matrixDet(int** matrix, int size);
int getSizeMatrix();
void getMatrixWithoutRowAndCol(int** matrix, int size, int row, int col, int** newMatrix);

