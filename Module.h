#pragma once
int** CreateMatrix();
void PrintMatrix(int** Matrix);
int* GetMaxDiagonal(int** Matrix);
void PrintMaxDiagonal(int** Matrix);
int* GetMaxUnderDiagonal(int** Matrix);
void ReduceTriangleMatrx(int** Matrix);
int** SumMatrix(int** MatrixA, int** MatrixB);